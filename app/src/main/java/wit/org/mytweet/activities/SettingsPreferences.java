package wit.org.mytweet.activities;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import wit.org.mytweet.R;

public class SettingsPreferences extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);
    }
}