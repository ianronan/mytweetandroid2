package wit.org.mytweet.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

public class Tweet
{
    public UUID id;
    public String tweetText;
    public Date date;
    public int numberCounter;
    public String contact;
    public String subject;

    private static final String JSON_ID             = "id"            ;
    private static final String JSON_TWEETTEXT      = "tweetText"     ;
    private static final String JSON_DATE           = "date"          ;
    private static final String JSON_NUMBERCOUNTER  = "numberCounter" ;

    public Tweet()
    {
        id = UUID.randomUUID();
        tweetText = "";
        date = new Date();
        numberCounter = 140;
        contact = "";
        subject = "";
    }

    public Tweet(JSONObject json) throws JSONException
    {
        id            = UUID.fromString(json.getString(JSON_ID));
        tweetText     = json.getString(JSON_TWEETTEXT);
        date          = new Date(json.getLong(JSON_DATE));
        numberCounter = json.getInt(JSON_NUMBERCOUNTER);
    }

    public JSONObject toJSON() throws JSONException
    {
        JSONObject json = new JSONObject();
        json.put(JSON_ID            , id.toString());
        json.put(JSON_TWEETTEXT     , tweetText);
        json.put(JSON_DATE          , date);
        json.put(JSON_NUMBERCOUNTER , numberCounter);
        return json;
    }

    public void setTweetText(String tweetText)
    {
        this.tweetText = tweetText;
    }

    public String getDateString()
    {
        return DateFormat.getDateTimeInstance().format(date);
    }

    public String getTweet()
    {
        String dateFormat = "EEE, MMM dd";
        String dateString = android.text.format.DateFormat.format(dateFormat, date).toString();

        String tweetInfo =  "Text " + tweetText + " Date: " + dateString;

        return tweetInfo;
    }
}
